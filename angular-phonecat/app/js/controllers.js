	'use strict';
	
	/* Controllers */
	
	var phonecatControllers = angular.module('phonecatControllers', []);
	
	phonecatControllers.controller('PhoneListCtrl', ['$scope', 'Phone',
	  function($scope, Phone) {
	  $scope.phones = Phone.query();
	   
	  $scope.phones = Phone.query(function(phones){
	      phones.forEach(function(phone){
		Phone.get({phoneId: phone.id}, function(phoneInfo){
		  phone.weight = parseFloat(phoneInfo.sizeAndWeight.weight);
		  phone.talktime = parseFloat(phoneInfo.battery.talkTime);
		  phone.stand = parseFloat(phoneInfo.battery.standbyTime);
		  phone.prime = parseFloat(phoneInfo.camera.primary);
		  console.log(phone.name + " : " + phone.primary);
		});
	      });
	  });
	    
	    
	   
	  $scope.orderProp = 'age';
	   
	  }]);
	
	
	phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
	  function($scope, $routeParams, Phone) {
	    $scope.phone1 = Phone.get({phoneId: $routeParams.phoneId}, function(phone1) {
	      $scope.mainImageUrl = phone1.images[0];
	    });
	
	    $scope.setImage = function(imageUrl) {
	      $scope.mainImageUrl = imageUrl;
	    }
	  }]);
	  
	phonecatControllers.controller('PhoneStuffCtrl', ['$scope', '$routeParams', 'Phone',
	  function($scope, $routeParams, Phone) {
	    $scope.phone1 = Phone.get({phoneId: $routeParams.phoneId1}, function(phone1) {
	      $scope.mainImageUrl = phone1.images[0];
	    });
		$scope.phone2 = Phone.get({phoneId: $routeParams.phoneId2}, function(phone2) {
	      $scope.mainImageUrl = phone2.images[0];
	    });
	    $scope.setImage = function(imageUrl) {
	      $scope.mainImageUrl = imageUrl;
	    }
	    
	  }]);
		
	  
	  
	
