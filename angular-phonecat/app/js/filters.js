'use strict';

/* Filters */

angular.module('phonecatFilters', []).filter('checkmark', function() {
  return function(input) {
    return input ? '\u2713' : '\u2718';
  };
}).filter('available', function() {
  return function(input) {
    return input ? input : 'not available';
  };
});
  
